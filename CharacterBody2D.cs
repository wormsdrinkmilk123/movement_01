using Godot;
using System;

public partial class CharacterBody2D : Godot.CharacterBody2D
{
	public const float Speed = 500.0f;
	public float JumpVelocity = -400.0f;
	public int JumpCounter;
	public float t;
	public Vector2 Momentum, direction;
	// Get the gravity from the project settings to be synced with RigidBody nodes.
	public float gravity = ProjectSettings.GetSetting("physics/2d/default_gravity").AsSingle();
	bool jumpJustPressed, jumpPressed;

	public override void _Process(double delta){
        GetInput();
    }

	void GetInput(){
		jumpPressed = Input.IsActionPressed("jump");
		jumpJustPressed = Input.IsActionJustPressed("jump");
		direction = (Input.GetVector("left", "right", "up", "down"));
	}


	public override void _PhysicsProcess(double delta)
	{
		Vector2 velocity = Velocity;

		// Add the gravity.
		if (!IsOnFloor())
			velocity.Y += gravity * (float)delta;

		// Handle Jump.
		if (jumpJustPressed) {
			if (IsOnFloor()) {
				velocity.Y = JumpVelocity;
			}
			JumpCounter += 1;
		}
		if (jumpPressed && t < 1 && JumpCounter < 1){
			velocity.Y = Mathf.MoveToward(JumpVelocity, JumpVelocity * 2, (float)delta * 2);
			t += 0.05f;
		}
		
		// Get the input direction and handle the movement/deceleration.
		// As good practice, you should replace UI actions with custom gameplay actions.
		if (IsOnFloor()) {
			Momentum.X = direction.X;
			Momentum.Y = direction.Y;
			t = 0.0f;
			JumpCounter = 0;
		}
		GD.Print(JumpCounter);
		
		if (!IsOnFloor() && direction != Momentum) 
		{
			direction *= new Vector2(0.3f, 0.3f);
		}
		else {
			direction *= 1;
		}
		if (direction != Vector2.Zero)
		{
			velocity.X = direction.X * Speed;
		}
		else
		{
			velocity.X = Mathf.MoveToward(Velocity.X, 0, Speed);
		}
		Velocity = velocity;
		MoveAndSlide();
	}
}
